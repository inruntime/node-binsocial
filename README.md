# Node library for BinSocial API

## Credits

This library is based on the work of:
* Paulius Uza.

## License

This code is free to use under the terms of the MIT license.
