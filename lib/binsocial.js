module.exports = (function() {

  var crypto = require('crypto');
  var request = require('request');

  var BinSocial = function(options) {
    this.options = options;
    return this;
  };

  /**
   * @private
   * Used for testing so that modules can be overidden.
   */

  BinSocial.prototype = {
    domain: 'api.binsocial.com',
    scheme: 'http',
    port: 80
  };

  return BinSocial;
  
})();